(function ($) {
    $(document).ready(function () {
        if ($(".has-zoom-animation").length > 0) {
            $(".has-zoom-animation").addClass("vc-animated");
        }
    });
})(jQuery);